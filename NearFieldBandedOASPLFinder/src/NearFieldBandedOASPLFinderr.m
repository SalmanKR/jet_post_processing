function [] = NearFieldBandedOASPLFinderr(bandfrequency)
%% Cleaning
format long g

%% User input
%bandfrequency = 20000;
obsrange = [0:1];

%% File Preparation
%Location of first simulation's files
obsrangesize = size(obsrange);

for i = 1:obsrangesize(1,2)
    file1 = strcat('../input/SPL_cpr-p2-p-pt',num2str(obsrange(1,i)),'.dat');
    B = importdata(file1);

    %% Finding Band Frequency
    %Finding the maximum size of the imported data set
    sz = size(B);

    %Initializing the band frequency row location
    bandfrequencyrowlocation = 1;

    %Looping through the entire imported data set to find the row location of
    %the frequency band
    for currentrow = 1:sz(1,1)
        %Initializing the lowest error variable
        if currentrow == 1
            lowesterror = abs(bandfrequency - B(currentrow,1));
        end

       %Finding the frequency error between the current frequency in the data
       %array and the desired frequency for the band
       currenterror = abs(bandfrequency - B(currentrow,1));

       %if the current frequency value is closer than the closest frequency we
       %have, change the row location to the current row
       if currenterror < lowesterror
           bandfrequencyrowlocation = currentrow;
           lowesterror = currenterror;
       end
    end

    %% Calculating the OASPL within the specified band
        %Isolating the band
        extractedband = B(1:bandfrequencyrowlocation,:);

        %Calculating the OASPL within the band
        BandedOASPL =  10*log10(sum(10.^(extractedband(:,2)/10)));

    %% Displaying the OASPL within the band
    message = ['ObsPoint: ', file1 ,' Frequency Band:',num2str(bandfrequency),' OASPL: ',num2str(BandedOASPL)];
    disp(message)
end
end

