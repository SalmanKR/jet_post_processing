#!/bin/bash
#
#rm -r /home/sal/Desktop/CDNozzle1.47/ICDSV3/M2_uncurved/p1-smooth/post-proc

rm /home/sal/Desktop/JetPostProc/DataRakePlotter/External/input1/*
rm /home/sal/Desktop/JetPostProc/DataRakePlotter/Internal/input1/*
rm /home/sal/Desktop/JetPostProc/DataRakePlotter/External/input2/*
rm /home/sal/Desktop/JetPostProc/DataRakePlotter/Internal/input2/*

rm /home/sal/Desktop/JetPostProc/DataRakePlotter/External/FiguresOutput/*
rm /home/sal/Desktop/JetPostProc/DataRakePlotter/Internal/FiguresOutput/*

rm /home/sal/Desktop/JetPostProc/MonotonicCleaner/monitor_data/*
rm /home/sal/Desktop/JetPostProc/MonotonicCleaner/extract_physical_data/*

rm /home/sal/Desktop/JetPostProc/SPLPlotter/FWHPlotter/FiguresOutput/*
rm /home/sal/Desktop/JetPostProc/SPLPlotter/MonitorPointPlotter/FiguresOutput/*

rm /home/sal/Desktop/JetPostProc/SPLPlotter/MonitorPointPlotter/input1/*
rm /home/sal/Desktop/JetPostProc/SPLPlotter/FWHPlotter/input1/*
rm /home/sal/Desktop/JetPostProc/SPLPlotter/MonitorPointPlotter/input2/*
rm /home/sal/Desktop/JetPostProc/SPLPlotter/FWHPlotter/input2/*

rm /home/sal/Desktop/JetPostProc/CoefficientAverager/input1/*
rm /home/sal/Desktop/JetPostProc/CoefficientAverager/input2/*

rm /home/sal/Desktop/JetPostProc/DataSplitter/input/*
rm /home/sal/Desktop/JetPostProc/DataSplitter/output/*

rm /home/sal/Desktop/JetPostProc/FarFieldBandedOASPLFinder/input/*
rm /home/sal/Desktop/JetPostProc/FarFieldBandedOASPLFinder/src/FarFieldOASPLOutput.log

rm /home/sal/Desktop/JetPostProc/NearFieldBandedOASPLFinder/input/*
rm /home/sal/Desktop/JetPostProc/NearFieldBandedOASPLFinder/src/NearFieldOASPLOutput.log
