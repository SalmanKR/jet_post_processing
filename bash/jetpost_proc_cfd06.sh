#!/bin/bash

echo Please enter the directory for input1. Note: it is assumed that all the necessary files have been genereated for input1
read input1

echo Please enter the name for input1
read name1

echo Please enter the directory for input2
read input2

echo Please enter the name for input2
read name2

echo Please enter the start time that you would like to use in the SPL calculations
read start_time

echo Please enter the number of halfing you would like to do to the near-field SPL, put 0 for none
read numofsplits

echo Please enter the Near-Field and Far-Field Frequency Band
read freqband

#External Rake Processing
echo Starting the external rake data processing
cd /home/sal/Desktop/JetPostProc/DataRakePlotter/External/src
"/home/sal/softwares/MATLAB/bin/matlab" -nodisplay -nosplash -nodesktop -r "ExternalRakePlotterFunc('$name1','$name2');exit;"

chmod u+x /home/sal/Desktop/JetPostProc/DataRakePlotter/External/FiguresOutput
echo Moving external rake figures to post-proc directory
cp -r /home/sal/Desktop/JetPostProc/DataRakePlotter/External/FiguresOutput/* $input2/post-proc

echo External rake figures moved to post-proc

#Internal Rake Processing
echo Starting internal rake data processing
cd /home/sal/Desktop/JetPostProc/DataRakePlotter/Internal/src
"/home/sal/softwares/MATLAB/bin/matlab" -nodisplay -nosplash -nodesktop -r "InternalRakePlotterFunc('$name1','$name2');exit;"

chmod u+x /home/sal/Desktop/JetPostProc/DataRakePlotter/Internal/FiguresOutput/*
echo Moving internal rake figures to post-proc directory
cp -r /home/sal/Desktop/JetPostProc/DataRakePlotter/Internal/FiguresOutput/* $input2/post-proc

echo Internal rake figures moved to post-proc

#Cleaning Near-Field Data
#Have to modify to do each monitor point
chmod u+x $input2/sol/monitor_points_*.dat

echo Moving monitorpoint file to post-proc
cp -r $input2/sol/monitor_points_*.dat $input2/post-proc

echo Cleaning near-field data
cp -r $input2/post-proc/monitor_points_*.dat /home/sal/Desktop/JetPostProc/MonotonicCleaner/monitor_data
cd /home/sal/Desktop/JetPostProc/MonotonicCleaner

"/home/sal/softwares/MATLAB/bin/matlab" -nodisplay -nosplash -nodesktop -r "extract_mon_pts_pt0Func($start_time);exit;"
"/home/sal/softwares/MATLAB/bin/matlab" -nodisplay -nosplash -nodesktop -r "extract_mon_pts_pt1Func($start_time);exit;"

echo Moving the cleaned monitor point data to post-proc directory
cp /home/sal/Desktop/JetPostProc/MonotonicCleaner/extract_physical_data/* $input2/post-proc/

#-------------------------------------------------------------------------------
echo Moving the cleaned monitor point data to the data splitter
cp $input2/post-proc/cpr-p2-p-pt*.dat /home/sal/Desktop/JetPostProc/DataSplitter/input
cd /home/sal/Desktop/JetPostProc/DataSplitter/src


echo Splitting the cleaned monitor point data
"/home/sal/softwares/MATLAB/bin/matlab" -nodisplay -nosplash -nodesktop -r "Row_Eliminator_pt0($numofsplits);exit;"

"/home/sal/softwares/MATLAB/bin/matlab" -nodisplay -nosplash -nodesktop -r "Row_Eliminator_pt1($numofsplits);exit;"

echo Moving the cleaned and split data to the post-proc directory
cp /home/sal/Desktop/JetPostProc/DataSplitter/output/* $input2/post-proc/

#Converting Monitor Point Data to SPL
echo Converting monitor point data to spl
cd $input2/post-proc
fftpsd -dft -i cpr-p2-p-pt0.dat -o ./ -l 0.0171083137623 -c 2 -w hann -variance -spl
fftpsd -dft -i cpr-p2-p-pt1.dat -o ./ -l 0.0171083137623 -c 2 -w hann -variance -spl
echo Monitor point data conversion complete

#Saving Plots of Near-Field Data
echo Creating and Storing Near-Field Data Plots
cp $input1/post-proc/SPL_cpr-p2-p-pt*.dat /home/sal/Desktop/JetPostProc/SPLPlotter/MonitorPointPlotter/input1
cp $input2/post-proc/SPL_cpr-p2-p-pt*.dat /home/sal/Desktop/JetPostProc/SPLPlotter/MonitorPointPlotter/input2
cd /home/sal/Desktop/JetPostProc/SPLPlotter/MonitorPointPlotter/src
"/home/sal/softwares/MATLAB/bin/matlab" -nodisplay -nosplash -nodesktop -r "SPLPlotterFunc('$name1','$name2');exit;"
cp /home/sal/Desktop/JetPostProc/SPLPlotter/MonitorPointPlotter/FiguresOutput/* $input2/post-proc
echo Near-Field Data Plots Created
#---------------------------------------------------------------------------

#Saving Plots of Far-Field Data
echo Creating Far-Field Waterfall Plot
cd /home/sal/Desktop/JetPostProc/SPLPlotter/FWHPlotter/src
cp $input1/sol/fwh_spl_ob* /home/sal/Desktop/JetPostProc/SPLPlotter/FWHPlotter/input1
cp $input2/sol/fwh_spl_ob* /home/sal/Desktop/JetPostProc/SPLPlotter/FWHPlotter/input2
"/home/sal/softwares/MATLAB/bin/matlab" -nodisplay -nosplash -nodesktop -r "FWHPlotter_WaterfallFunc('$name1','$name2');exit;"

echo Creating Individual Far-Field Plots
"/home/sal/softwares/MATLAB/bin/matlab" -nodisplay -nosplash -nodesktop -r "FWHPlotter_90Func('$name1','$name2');exit;"
"/home/sal/softwares/MATLAB/bin/matlab" -nodisplay -nosplash -nodesktop -r "FWHPlotter_120Func('$name1','$name2');exit;"
"/home/sal/softwares/MATLAB/bin/matlab" -nodisplay -nosplash -nodesktop -r "FWHPlotter_150Func('$name1','$name2');exit;"
cp /home/sal/Desktop/JetPostProc/SPLPlotter/FWHPlotter/FiguresOutput/* $input2/post-proc/
echo Far-Field Plots Created

#Finding Time-Averaged Flow Coefficient Values
#echo Displaying Time-Averaged Coefficient Values
#cp $input1/sol/output.dat /home/sal/Desktop/JetPostProc/CoefficientAverager/input1
#cp $input2/sol/output.dat /home/sal/Desktop/JetPostProc/CoefficientAverager/input2

#cd /home/sal/Desktop/JetPostProc/CoefficientAverager/src
#"/home/sal/softwares/MATLAB/bin/matlab" -nodisplay -nosplash -nodesktop -r "Coefficient_Averager('$name1','$name2');exit;"

#Finding OASPL for Near-Field and Far-Field
mv $input2/sol/fwh_spl_ob*.dat /home/sal/Desktop/JetPostProc/FarFieldBandedOASPLFinder/input
cd /home/sal/Desktop/JetPostProc/FarFieldBandedOASPLFinder/src
"/home/sal/softwares/MATLAB/bin/matlab" -nodisplay -nosplash -nodesktop -r "FarFieldBandedOASPLFinderr($freqband);exit;" >> ./FarFieldOASPLOutput.log
mv ./FarFieldOASPLOutput.log $input2/post-proc

mv $input2/post-proc/SPL_cpr-p2-p-pt*.dat /home/sal/Desktop/JetPostProc/NearFieldBandedOASPLFinder/input
cd /home/sal/Desktop/JetPostProc/NearFieldBandedOASPLFinder/src
"/home/sal/softwares/MATLAB/bin/matlab" -nodisplay -nosplash -nodesktop -r "NearFieldBandedOASPLFinderr($freqband);exit;" >> NearFieldOASPLOutput.log
mv ./NearFieldOASPLOutput.log $input2/post-proc
