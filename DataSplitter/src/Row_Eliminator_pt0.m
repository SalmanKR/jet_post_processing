function [] = Row_Eliminator(numofsplits)

format long g

%Specifying the file location
File1 = '../input/cpr-p2-p-pt0.dat';

%Importing the file contents
B = importdata(File1);

%Specifying to cut every second line out
n = 2;

%Initializing the loop to cut for the specified amount of cuts
i = 0;
while i < numofsplits
    B(n:n:end,:) = [];
    i = i + 1;
end

%Writing the new trimmed data to a new file
dlmwrite('../output/cpr-p2-p-pt0.dat',B,' ')
end
