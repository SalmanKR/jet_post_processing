function [] = Coefficient_Averager(SimulationTitle1,SimulationTitle2)
    %FUNCTION DEVELOPMENT
    avgstart_row = 1000;

    %Importing the data from the coefficients file
    file1 = '../input1/output.dat'
    file2 = '../input2/output.dat'
    A = importdata(file1);
    B = importdata(file2);

    %Removing the excess rows from the data matrices
    A.data([1:avgstart_row,1],:) = [];
    B.data([1:avgstart_row,1],:) = [];

    %Getting the means of each of the data arrays
    Means1 = mean(A.data,1);
    Means2 = mean(B.data,1);

    %Displaying the values
    sz = size(A.textdata);

    for i = 1:sz(1,2)
        disp(A.textdata(1,i))
        disp(strcat(SimulationTitle1,' :', num2str(Means1(1,i))))
        disp(strcat(SimulationTitle2,' :', num2str(Means2(1,i))))
        disp(' ')
        disp(' ')
    end
end

